<?php
/*
Plugin Name: University Post Types
Plugin URI: http://fictional-university.test/
Description: Custom post types for Fictional University
Author: Andy Brooks
Version: 1.0
Author URI: http://www.icthussolutions.com/
*/

function university_post_types() {
    $event_label_singular = 'Event';
	$event_label_plural   = 'Events';
  register_post_type('university_event', array(
    
    'public' => true,
    'has_archive' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'description' => '',
    'labels' => array(
        'name'               => $event_label_plural,
        'singular_name'      => $event_label_singular,
        'menu_name'          => $event_label_plural,
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New ' . $event_label_singular,
        'edit'               => 'Edit',
        'edit_item'          => 'Edit ' . $event_label_singular,
        'new_item'           => 'New ' . $event_label_singular,
        'view'               => 'View ' . $event_label_singular,
        'view_item'          => 'View ' . $event_label_singular,
        'search_items'       => 'Search ' . $event_label_plural,
        'not_found'          => 'No ' . $event_label_plural . ' Found',
        'not_found_in_trash' => 'No ' . $event_label_plural . ' Found in Trash',
        'parent'             => 'Parent ' . $event_label_singular,
    ),
    'menu_icon' => 'dashicons-calendar-alt',
    'capability_type' => 'event',
    'map_meta_cap' => true,
    'supports'  => array(
        'excerpt',
        'editor',
        'title'
    ),
    'rewrite' => array(
        'with_front' => false,
        'slug'       => 'events'
    )
  ));

  // University Program Post Type

  $program_label_singular = 'Program';
  $program_label_plural   = 'Programs';
  register_post_type('university_program', array(
    
    'public' => true,
    'has_archive' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'description' => '',
    'labels' => array(
        'name'               => $program_label_plural,
        'singular_name'      => $program_label_singular,
        'menu_name'          => $program_label_plural,
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New ' . $program_label_singular,
        'edit'               => 'Edit',
        'edit_item'          => 'Edit ' . $program_label_singular,
        'new_item'           => 'New ' . $program_label_singular,
        'view'               => 'View ' . $program_label_singular,
        'view_item'          => 'View ' . $program_label_singular,
        'search_items'       => 'Search ' . $program_label_plural,
        'not_found'          => 'No ' . $program_label_plural . ' Found',
        'not_found_in_trash' => 'No ' . $program_label_plural . ' Found in Trash',
        'parent'             => 'Parent ' . $program_label_singular,
    ),
    'menu_icon' => 'dashicons-awards',
    'supports'  => array(
        'excerpt',
        'title'
    ),
    'rewrite' => array(
        'with_front' => false,
        'slug'       => 'programs'
    )
  ));

  // University Professor Post Type

  $professor_label_singular = 'Professor';
  $professor_label_plural   = 'Professors';
  register_post_type('university_professor', array(
    
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'description' => '',
    'labels' => array(
        'name'               => $professor_label_plural,
        'singular_name'      => $professor_label_singular,
        'menu_name'          => $professor_label_plural,
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New ' . $professor_label_singular,
        'edit'               => 'Edit',
        'edit_item'          => 'Edit ' . $professor_label_singular,
        'new_item'           => 'New ' . $professor_label_singular,
        'view'               => 'View ' . $professor_label_singular,
        'view_item'          => 'View ' . $professor_label_singular,
        'search_items'       => 'Search ' . $professor_label_plural,
        'not_found'          => 'No ' . $professor_label_plural . ' Found',
        'not_found_in_trash' => 'No ' . $professor_label_plural . ' Found in Trash',
        'parent'             => 'Parent ' . $professor_label_singular,
    ),
    'menu_icon' => 'dashicons-welcome-learn-more',
    'supports'  => array(
        'excerpt',
        'editor',
        'title',
        'thumbnail'
    ),
    'show_in_rest'  => true,
    'rewrite' => array(
        'with_front' => false,
        'slug'       => 'professors'
    )
  ));

  // My Notes Post Type

  $note_label_singular = 'Note';
  $note_label_plural   = 'Notes';
  register_post_type('notes', array(
    'capability_type' => 'notes',
    'map_meta_cap' => true,
    'public' => false,
    'show_ui' => true,
    'description' => '',
    'labels' => array(
        'name'               => $note_label_plural,
        'singular_name'      => $note_label_singular,
        'menu_name'          => $note_label_plural,
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New ' . $note_label_singular,
        'edit'               => 'Edit',
        'edit_item'          => 'Edit ' . $note_label_singular,
        'new_item'           => 'New ' . $note_label_singular,
        'view'               => 'View ' . $note_label_singular,
        'view_item'          => 'View ' . $note_label_singular,
        'search_items'       => 'Search ' . $note_label_plural,
        'not_found'          => 'No ' . $note_label_plural . ' Found',
        'not_found_in_trash' => 'No ' . $note_label_plural . ' Found in Trash',
        'parent'             => 'Parent ' . $note_label_singular,
    ),
    'menu_icon' => 'dashicons-welcome-write-blog',
    'supports'  => array(
        'editor',
        'title'
    ),
    'show_in_rest'  => true,
    'rewrite' => array(
        'with_front' => false,
        'slug'       => 'notes'
    )
  ));

  // Campus Post Type

  $campus_label_singular = 'Campus';
  $campus_label_plural   = 'Campuses';
  register_post_type('university_campus', array(
    
    'public' => true,
    'has_archive' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'description' => '',
    'labels' => array(
        'name'               => $campus_label_plural,
        'singular_name'      => $campus_label_singular,
        'menu_name'          => $campus_label_plural,
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New ' . $campus_label_singular,
        'edit'               => 'Edit',
        'edit_item'          => 'Edit ' . $campus_label_singular,
        'new_item'           => 'New ' . $campus_label_singular,
        'view'               => 'View ' . $campus_label_singular,
        'view_item'          => 'View ' . $campus_label_singular,
        'search_items'       => 'Search ' . $campus_label_plural,
        'not_found'          => 'No ' . $campus_label_plural . ' Found',
        'not_found_in_trash' => 'No ' . $campus_label_plural . ' Found in Trash',
        'parent'             => 'Parent ' . $campus_label_singular,
    ),
    'menu_icon' => 'dashicons-location-alt',
    'capability_type' => 'campus',
    'map_meta_cap' => true,
    'supports'  => array(
        'excerpt',
        'editor',
        'title',
        'thumbnail'
    ),
    'rewrite' => array(
        'with_front' => false,
        'slug'       => 'campuses'
    )
  ));

  // Professor Likes Post Type

  $like_label_singular = 'Like';
  $like_label_plural   = 'Likes';
  register_post_type('like', array(
    'public' => false,
    'show_ui' => true,
    'description' => '',
    'labels' => array(
        'name'               => $like_label_plural,
        'singular_name'      => $like_label_singular,
        'menu_name'          => $like_label_plural,
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New ' . $like_label_singular,
        'edit'               => 'Edit',
        'edit_item'          => 'Edit ' . $like_label_singular,
        'new_item'           => 'New ' . $like_label_singular,
        'view'               => 'View ' . $like_label_singular,
        'view_item'          => 'View ' . $like_label_singular,
        'search_items'       => 'Search ' . $like_label_plural,
        'not_found'          => 'No ' . $like_label_plural . ' Found',
        'not_found_in_trash' => 'No ' . $like_label_plural . ' Found in Trash',
        'parent'             => 'Parent ' . $like_label_singular,
    ),
    'menu_icon' => 'dashicons-heart',
    'supports'  => array(
        'title'
    )
  ));

}

add_action('init', 'university_post_types');

register_deactivation_hook( __FILE__, 'flush_rewrite_rules' );
register_activation_hook( __FILE__, 'uni_flush_rewrites' );

function uni_rewrite_rules() {
    university_post_types();

    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'uni_rewrite_rules' );