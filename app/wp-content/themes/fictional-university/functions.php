<?php
require get_theme_file_path('/inc/search-route.php');
require get_theme_file_path('/inc/like-route.php');

function university_custom_rest() {
  register_rest_field('post', 'authorName', array(
    'get_callback'  =>  function() {
      return get_the_author();
    }
  ));

  register_rest_field('notes', 'userNoteCount', array(
    'get_callback'  =>  function() {
      return count_user_posts(get_current_user_id(), 'notes');
    }
  ));

}

add_action('rest_api_init', 'university_custom_rest');

function pageBanner($args = NULL) {
  // PHP Logic will go here

  if (!isset($args['title'])) {
    $args['title'] = get_the_title();
  }

  if (!isset($args['subtitle'])) {
    $args['subtitle'] = get_field('page_banner_sub-title');
  }

  if (!isset($args['photo'])) {
    $pageBannerImage = get_field('page_banner_background_image');
    if ($pageBannerImage) {
      $args['photo'] = $pageBannerImage['sizes']['pageBanner'];
    } else {
      $args['photo'] = get_theme_file_uri('images/ocean.jpg');
    }
  }

?>
        <div class="page-banner">
          <div class="page-banner__bg-image" style="background-image: url(<?php echo $args['photo']; ?>)">
          </div>
          <div class="page-banner__content container container--narrow">
            <h1 class="page-banner__title"><?php echo $args['title']; ?></h1>
            <div class="page-banner__intro">
              <p><?php echo $args['subtitle']; ?></p>
            </div>
          </div>  
        </div>
<?php
}

function university_files() {
  
  wp_enqueue_script( 'google-maps-js', '//maps.googleapis.com/maps/api/js?key=AIzaSyBKtjMA5yGwiCd6WMrv7bnWwoZdcEAvQ1Y', NULL, '1.0', true);

  wp_enqueue_script( 'main-university-js', get_theme_file_uri('/js/scripts-bundled.js'), NULL, 'microtime()', true);

  wp_enqueue_style( 'custom-google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i' );

  wp_enqueue_style( 'font-awesome','//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );

  wp_enqueue_style('university-main-styles', get_stylesheet_uri(), NULL, microtime());

  wp_localize_script('main-university-js', 'universityData', array(
    'root_url' => get_site_url(),
    'nonce' => wp_create_nonce( 'wp_rest' )
  ));
}

add_action('wp_enqueue_scripts', 'university_files');

function university_features() {
  register_nav_menus(
    array(
    'headerMenuLocation' => __('Header Menu Location'),
    'footerMenuLocation1' => __('Footer Menu Location 1'),
    'footerMenuLocation2'=> __('Footer Menu Location 2')
    )
  );

  add_theme_support( 'title-tag' );
  add_theme_support( 'post-thumbnails' );
  add_image_size('professorLandscape', 400, 260, true);
  add_image_size('professorPortrait', 480, 650, true);
  add_image_size('pageBanner', 1500, 350, true);

}
add_action( 'after_setup_theme', 'university_features' );

function university_adjust_queries($query) {

  

  if ( !is_admin() AND is_post_type_archive('university_event') AND $query->is_main_query() ) {
    $today = date('Ymd');
    $query->set('meta_key', 'event_date');
    $query->set('orderby', 'meta_value_num');
    $query->set('order', 'ASC');
    $query->set('meta_query', array(
      array(
        'key'         =>  'event_date',
        'compare'     =>  '>=',
        'value'       =>  $today,
        'type'        =>  'numeric'
      )
      )
    );
  }

  if (!is_admin() AND is_post_type_archive('university_program') AND $query->is_main_query()) {
    $query->set('posts_per_page', -1);
    $query->set('orderby', 'title');
    $query->set('order', 'ASC');
  }

  if (!is_admin() AND is_post_type_archive('university_campus') AND $query->is_main_query()) {
    $query->set('posts_per_page', -1);
  }
}
add_action('pre_get_posts', 'university_adjust_queries');

add_action('nav_menu_css_class', 'add_current_nav_class', 10, 2 );
	
	function add_current_nav_class($classes, $item) {
		
		// Getting the current post details
		global $post;
		
		// Getting the post type of the current post
		$current_post_type = get_post_type_object(get_post_type($post->ID));
		$current_post_type_slug = $current_post_type->rewrite['slug'];
			
		// Getting the URL of the menu item
		$menu_slug = strtolower(trim($item->url));
		
		// If the menu item URL contains the current post types slug add the current-menu-item class
		if (strpos($menu_slug,$current_post_type_slug) !== false) {
		
		  $classes[] = 'current-menu-item';
		
		}
		
		// Return the corrected set of classes to be added to the menu item
		return $classes;
	
}
function universityMapKey($api) {
  $api['key'] = 'AIzaSyBKtjMA5yGwiCd6WMrv7bnWwoZdcEAvQ1Y';
  return $api;
}
add_filter('acf/fields/google_map/api', 'universityMapKey');

// Redirect subscriber accounts out of admin and onto homepage
add_action('admin_init', 'redirectSubsToFrontend');

function redirectSubsToFrontend() {
  $ourCurrentUser = wp_get_current_user();

  if (count($ourCurrentUser->roles) == 1 AND $ourCurrentUser->roles[0] == 'subscriber') {
    wp_redirect(site_url('/'));
    exit;
  }
}

add_action('wp_loaded', 'noSubsAdminBar');

function noSubsAdminBar() {
  $ourCurrentUser = wp_get_current_user();

  if (count($ourCurrentUser->roles) == 1 AND $ourCurrentUser->roles[0] == 'subscriber') {
    show_admin_bar(false);
  }
}

// Customise Login screen
function ourHeaderURL() {
  return esc_url(site_url('/'));
}
add_filter('login_headerurl', 'ourHeaderURL');

function ourLoginLogoTitle() {
  return get_bloginfo('name');
}
add_filter( 'login_headertitle', 'ourLoginLogoTitle' );

function ourLoginCSS() {
  wp_enqueue_style( 'custom-google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i' );
  wp_enqueue_style('university-main-styles', get_stylesheet_uri());
}
add_action('login_enqueue_scripts', 'ourLoginCSS');

// Force Note posts to be private
add_filter('wp_insert_post_data', 'makeNotePrivate', 10, 2);

function MakeNotePrivate($data, $postarr) {
  // Setup a maximum per user post limit
  if($data['post_type'] == 'notes') {
    if(count_user_posts( get_current_user_id(), 'notes') > 4 AND !$postarr['ID']) {
      die('You have reached your note limit.');
    }
    $data['post_title'] = sanitize_text_field($data['post_title']);
    $data['post_content'] = sanitize_textarea_field($data['post_content']);
  }
  if($data['post_type'] == 'notes' AND $data['post_status'] != 'trash') {
    $data['post_status'] = 'private';
  }
  
  return $data;
}

