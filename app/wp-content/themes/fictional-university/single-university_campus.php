<?php
  
  get_header();

      while(have_posts()) {
        the_post(); 
        pageBanner(array(
          'subtitle'  =>  'A stunning campus in the heart of Cardiff'
        ));
        ?>
        
          
    <div class="container container--narrow page-section">
    <div class="metabox metabox--position-up metabox--with-home-link">
        <p><a class="metabox__blog-home-link" href="<?php echo get_post_type_archive_link('university_campus'); ?>"><i class="fa fa-home" aria-hidden="true"></i> Back to <?php echo __('All Campuses'); ?></a> <span class="metabox__main"><?php the_title(); ?></span></p>
        </div>
      <div class="generic-content">
        <?php the_content(); ?>
      </div>
      <?php $mapLocation = get_field('campus_location'); ?>
      <div class="acf-map">
        <div class="marker" data-lat="<?php echo $mapLocation['lat']; ?>" data-lng="<?php echo $mapLocation['lng']; ?>">
            <h3><?php the_title(); ?></h3>
            <?php echo $mapLocation['address']; ?>
        </div>
      </div>

      <?php

        $relatedPrograms = new WP_Query(array(
          'post_type'       =>  'university_program',
          'posts_per_page'  =>  -1,
          'orderby'         =>  'title',
          'order'           =>  'ASC',
          'meta_query'      =>  array(
            array(
              'key'         =>  'related_campuses',
              'compare'     =>  'LIKE',
              'value'       =>  '"' . get_the_ID() .'"'
            )
          )
        ));



        if ( $relatedPrograms->have_posts() ) :
          echo '<hr class="section-break">';
          echo '<h2 class="headline headline--medium p-bottom-small">Programs related to this Campus</h2>';
          echo '<ul class="link-list min-list">';
          while($relatedPrograms -> have_posts()) {
            $relatedPrograms -> the_post(); ?>
              <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
        <?php
          
          }
          echo '</ul>';

        else :
          echo '<hr class="section-break">';
          echo '<p>There are no campuses for this program</p>';
        endif;

      wp_reset_postdata();

      
      ?>
      


      
      
    </div>


    
    
  <?php }

  get_footer();

?>