<?php get_header(); 
pageBanner(array(
  'title' =>  'All Professors',
  'subtitle'  =>  'All our esteemed professors.'
));
?>

<div class="container container--narrow page-section">

  <?php
    if (have_posts()) : ?>
       <ul class="link-list min-list">  
        <?php while(have_posts()) {
        the_post(); ?>
            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
        <?php } ?>
        </ul>
        <?php
    endif;
  ?>
  
</div>

<?php get_footer(); ?>