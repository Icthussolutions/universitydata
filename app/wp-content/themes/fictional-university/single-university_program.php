<?php
  
  get_header();
  pageBanner();

      while(have_posts()) {
        the_post(); ?>
        
    <div class="container container--narrow page-section">
      <div class="metabox metabox--position-up metabox--with-home-link">
        <p><a class="metabox__blog-home-link" href="<?php echo get_post_type_archive_link('university_program'); ?>"><i class="fa fa-home" aria-hidden="true"></i> Back to <?php echo __('All Programs'); ?></a> <span class="metabox__main"><?php the_title(); ?></span></p>
        </div>
      <div class="generic-content">
      <?php the_field('main_body_content'); ?>
      </div>
      

      <?php

        $relatedProfessors = new WP_Query(array(
          'post_type'       =>  'university_professor',
          'posts_per_page'  =>  -1,
          'orderby'         =>  'title',
          'order'           =>  'ASC',
          'meta_query'      =>  array(
            array(
              'key'         =>  'related_programs',
              'compare'     =>  'LIKE',
              'value'       =>  '"' . get_the_ID() .'"'
            )
          )
        ));



        if ( $relatedProfessors->have_posts() ) :
          echo '<hr class="section-break">';
          echo '<h2 class="headline headline--medium p-bottom-small">' . get_the_title() . ' Professors</h2>';
          echo '<ul class="professor-cards">';
          while($relatedProfessors -> have_posts()) {
            $relatedProfessors -> the_post(); ?>
              <li class="professor-card__list-item"><a class="professor-card" href="<?php the_permalink(); ?>">
                <img class="professor-card__image" src="<?php the_post_thumbnail_url('professorLandscape'); ?>" alt="">
                <span class="professor-card__name"><?php the_title(); ?></span>
              </a></li>
        <?php
          
          }

          wp_reset_postdata();

          echo '</ul>';

        else :
          echo '<hr class="section-break">';
          echo '<p>There are no professors for this program</p>';
        endif;


        $today = date('Ymd');
        $relatedEvents = new WP_Query(array(
          'post_type'       =>  'university_event',
          'posts_per_page'  =>  2,
          'meta_key'        =>  'event_date',
          'orderby'         =>  'meta_value_num',
          'order'           =>  'ASC',
          'meta_query'      =>  array(
            array(
              'key'         =>  'event_date',
              'compare'     =>  '>=',
              'value'       =>  $today,
              'type'        =>  'numeric'
            ),
            array(
              'key'         =>  'related_programs',
              'compare'     =>  'LIKE',
              'value'       =>  '"' . get_the_ID() .'"'
            )
          )
        ));

        

        if ( $relatedEvents->have_posts() ) :
          echo '<hr class="section-break">';
          echo '<h2 class="headline headline--medium p-bottom-small">Upcoming ' . get_the_title() . ' Events</h2>';

          while($relatedEvents -> have_posts()) {
            $relatedEvents -> the_post();
            
            get_template_part('template-parts/content', 'event');

          
          }

        else :
          echo '<hr class="section-break">';
          echo '<p>There are no upcoming events for this program</p>';
        endif;

        wp_reset_postdata();

        $relatedCampuses = get_field('related_campuses');

        if ($relatedCampuses) {
          echo '<hr class="section-break">';
          echo '<h2 class="headline headline--medium p-bottom-small">' . get_the_title() . ' is available at these Campuses:</h2>';

          echo '<ul class="link-list min-list">';

          foreach($relatedCampuses as $campus) { ?>
            <li><a href="<?php echo get_the_permalink($campus);  ?>"><?php echo get_the_title($campus); ?></a></li>
      <?php
          }
          echo '</ul>';

        }
      ?>
      
    </div>


    
    
  <?php }

  get_footer();

?>